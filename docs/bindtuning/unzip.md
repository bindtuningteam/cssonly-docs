The themes are provided in 2 different packaging all following Microsoft guidelines:

For **Classic Experience**: 

 - ***Sandbox Solution***, for Sandbox environments (*.wsp).

For **Modern Experience**: 

 - ***Modern Solution***, for Modern SharePoint (*.sppkg).

After unzipping your Theme package, you will find two zip files, a **SPFx package - Modern SharePoint** and **Sandbox Solution (SPCSS) - Classic SharePoint**. 
Depending on the SharePoint configuration you may install all the packages. 

![zip.png](../images/zip.png)

-------------

**Sandbox Solution** (Classic SharePoint) 📁

── yourthemename.SPCSS.wsp

-------------

**Modern Solution** (Modern SharePoint) 📁

![themestructure.png](../images/themestructure.png)

── THEME_NAME.json<br>
── THEME_NAME.spcolor<br>
── THEME_NAME.spfx.sppkg<br>
── Installer.ps1<br>

---------
