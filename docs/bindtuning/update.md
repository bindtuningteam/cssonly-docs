To update the BindTuning theme on your SharePoint site you need first to update them on your BindTuning account.

1. Login on your **BindTuning account** at BindTuning;
2. Mouse over the gravatar and select the **My Downloads**;
3. Select the **Theme tab**;
4. Mouse over the desired Theme and **View theme** details. If an update is available will show the blue pulsing icon and proceed as follows, otherwise move to **step 5**;

    ![update-theme.png](..\images\update-theme.png)

    - Click the Update to version X.X.X.X button to get the latest Theme files.
        
        ![updatetheme.png](..\images\updatetheme.png)

5. Click to **Download the theme**.

    ![download-theme.png](..\images\download-theme.png)