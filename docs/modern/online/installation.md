If you've already installed the theme previously, please move to the step to [Install/Update](#install) the theme.

1. Right click the **Instaler.ps1** file and select **Run with PowerShell** (run as administrator);
2. Select **option 4 - Install SharePoint PnP PowerShell *Online* Cmdlets**;

	![1-pnp-install.png](../../images/online/1-pnp-install.png)

3. Insert **A** or **Y** to allow installating the **SharePoint PnP PowerShell *Online* Cmdlets**.

	![2-repository.png](../../images/online/2-repository.png)

---
<a name="install"></a>
### Installing the theme on a site collection

1. If you're not running from the previous section Right click the **Instaler.ps1** file and select **Run with PowerShell**;
2. Select **option 1 or 2**;
	
	<p class="alert alert-success">Full color experience applies theme colors to Modern Page elements. To apply the theme colors, you must go to "Change the Look" gallery and select from Company themes the theme.</p>
	
3. Insert the site collection link where the theme should be applied;

4. On this step you can decide either to apply the theme for the Tenant App catalog or for the Site Collection App catalog;

	![9-menu2-insert-url.png](../../images/online/9-menu2-insert-url.png)

5. Login with your Office credentials;

	![10-login-on-office.png](../../images/online/10-login-on-office.png)

Theme installed on the Site! ✅ 

---

<a name="install"></a>
### Installing the theme on a HUB 

<p class="alert alert-info"> What is a SharePoint hub site? <br>SharePoint hub sites help you meet the needs of your organization by connecting and organizing sites based on project, department, division, region, etc. making it easier to... <a href="https://support.office.com/en-us/article/what-is-a-sharepoint-hub-site-fe26ae84-14b7-45b6-a6d1-948b3966427f" target="_blank">read more</a></p>

1. If you're not running from the previous section Right click the **Instaler.ps1** file and select **Run with PowerShell**;
2. Select **option 3 or 4**;
	
	<p class="alert alert-success">Full color experience applies theme colors to Modern Page elements. To apply the theme colors, you must go to "Change the Look" gallery and select from Company themes the theme.</p>
	
3. Insert the HUB link where the theme should be applied;

	![9.1-menu4-insert-url.png](../../images/online/9.1-menu4-insert-url.png)

4. Login with your Office credentials;

	![10.1-login-on-office.png](../../images/online/10.1-login-on-office.png)


Theme installed on all sites of the selected HUB! ✅ 

---

### Apply the Theme

If you've already the theme available for your tenant you can just apply the theme to any SharePoint Site collection you want.

1. If you're not running form the previous section Right click the **Instaler.ps1** file and select **Run with PowerShell**;
2. Select **option 5 or 6**;
	
	<p class="alert alert-success">Full color experience applies theme colors to Modern Page elements. To apply the theme colors, you must go to "Change the Look" gallery and select from Company themes the theme.</p>
	
3. Insert the site collection link where the theme should be applied;

4. On this step you can decide either to apply the theme for the Tenant App catalog or for the Site Collection App catalog;

	![9-menu2-insert-url.png](../../images/online/9-menu2-insert-url.png)

5. Login with your Office credentials;

	![10-login-on-office.png](../../images/online/10-login-on-office.png)

Theme applied to a Site! ✅ 