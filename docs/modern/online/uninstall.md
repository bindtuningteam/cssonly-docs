### Deactivate the theme

1. Right click the **Instaler.ps1** file and select **Run with PowerShell**;
2. Select **option 7**;
3. Select **option 1** to deactivate from a site collecion or **option 2** from a HUB

    ![9-menu7-deactivate.png](../../images/online/9-menu7-deactivate.png)

4. Insert the site link where the theme should be deactivated;
5. Login with your Office credentials;

    ![10-menu7-login-on-office.png](../../images/online/10-menu7-login-on-office.png)

Theme deactivated! ✅ 

---
### Uninstall theme

1. Right click the **Instaler.ps1** file and select **Run with PowerShell**;
2. Select **option 8**;
3. Insert the site collection link where the theme was installed or the root of your SharePoint site;
4. Login with your Office credentials;

    ![10-menu8-login-on-office.png](../../images/online/10.1-login-on-office.png)

5. We'll check where the solution is installed, either Tenant App Catalog or Site Collection App Catalog, so then you can choose from where to uninstall.

    ![uninstall-theme.png](../../images/online/uninstall-theme.png)
    
<p class="alert alert-danger">If the theme is uninstalled from the Tenant App Catalog and the theme is installed in multiple site collections, they will return to default theme.</p>

Theme uninstalled! ✅