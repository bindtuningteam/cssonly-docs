If you've already installed the theme previously, please move to the step to [Install/Update](#install) the theme.

1. Right click the **Instaler.ps1** file and select **Run with PowerShell** (run as administrator);
2. Select **option 4 - Install SharePoint PnP PowerShell *Online* Cmdlets**;

	![1-pnp-install.png](../../images/onprem/1-pnp-install.png)

3. Insert **A** or **Y** to allow installating the **SharePoint PnP PowerShell *Online* Cmdlets**.

	![2-repository.png](../../images/onprem/2-repository.png)

---
<a name="install"></a>
### Installing the theme on a site collection

1. Add the **themename.sppkg** to your App Catalogue. 

	![1-appcatalogue.png](../../images/onprem/1-appcatalogue.png)

2. Right click the **Instaler.ps1** file and select **Run with PowerShell**;
3. Select **option 1 or 2**;
	
	<p class="alert alert-success">Full color experience applies theme colors to Modern Page elements. To apply the theme colors, you must go to "Change the Look" gallery and select from Company themes the theme.</p>
	
4. Insert the site collection link where the theme should be applied;

5. Login with your SharePoint credentials;

	![10-login-on-office.png](../../images/onprem/10.1-login-on-office.png)

Theme installed on the Site! ✅ 

---

### Apply the Theme

If you've already the theme available for your tenant you can just apply the theme to any SharePoint Site collection you want.

1. If you're not running form the previous section Right click the **Instaler.ps1** file and select **Run with PowerShell**;
2. Select **option 1 or 2**;
	
	<p class="alert alert-success">Full color experience applies theme colors to Modern Page elements. To apply the theme colors, you must go to "Change the Look" gallery and select from Company themes the theme.</p>
	
3. Insert the site collection link where the theme should be applied;

4. Login with your Office credentials;

	![10.1-login-on-office.png](../../images/onprem/10.1-login-on-office.png)

Theme applied to a Site! ✅ 