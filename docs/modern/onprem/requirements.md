### Platform version
- SharePoint 2019

### Browsers

BindTuning Theme work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- Edge
- IE

### SharePoint requirements 

- An active **BindTuning Theme** Final or Trial;
- Windows 7 or above with Windows Management Framework v4;
- [PowerShell 4 or above](https://docs.microsoft.com/powershell/);
- [SharePoint Online PnP Cmdlets](https://github.com/SharePoint/PnP-PowerShell);
- [Tenant App Catalog or Site Collection App Catalog](https://support.office.com/en-us/article/use-the-app-catalog-to-make-custom-business-apps-available-for-your-sharepoint-online-environment-0b6ab336-8b83-423f-a06b-bcc52861cba0);
- Tenant App Catalog or Site Collection App Catalog Admin rights, depending on your configuration;
- [Set Execution Policy](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-6) to **Unrestricted**, you can set the default value after installing the theme.