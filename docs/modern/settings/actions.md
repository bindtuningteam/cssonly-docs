Quick actions

<img src="../../images/bindtuning-settings-actions.png" alt="BindTuning Settings Actions"	title="BindTuning Settings Actions" width="300" />

---

#### SharePoint Actions

- <b>Create site</b>;
- <b>Members</b>;
- <b>Share site</b>;
- <b>Follow site</b>;
- <b>Next steps</b>;

---
