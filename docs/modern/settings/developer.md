Settings for developers

<img src="../../images/bindtuning-settings-developer.png" alt="BindTuning Settings Developer"	title="BindTuning Settings Developer" width="300" />

---

#### Custom CSS
Allows you to add your custom CSS to the page

---

#### Custom Script
Allows you to add your custom script to the page. Loads after the scripts in addicional resources, so that you can use whatever your loading.

---

#### Additional Resources
Allows you to add CSS or JS files to your theme. These files load synchronously in the order you insert them. Only after all your script files have loaded does the custom script run.