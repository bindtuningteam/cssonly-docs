Settings affecting the look and feel of the page.

<img src="../../images/bindtuning-settings-general.png" alt="BindTuning Settings General"	title="BindTuning Settings General" width="300" />

---

#### Favicon

Sets a custom Favicon for your site collection. 

<p class="alert alert-info"><b>Not sure what a Favicon is?</b> <br>
Favicon is an icon that is picked up from the website to accompany the site name in the browser tabs and the Bookmarks/Favorites bar.</p>

![16-favicon.png](../../images/16.favicon.png)

---

#### Logo Link URL

Sets a custom URL for your logo to link to. Leave empty if you don't want your logo to link anywhere.

---

#### Compact Styling

This will reduce the spacing between elements like the logo and navbar, paddings in the footer and header and possibly other elements. It works to increase the available content area.

---

#### Hide native hub navigation

Hides native hub navigation

---

#### Hub navigation position

Have the hub navigation after or before the theme header

---

#### Hide native search

Hide native SharePoint search

---

#### Hide sidebar

Hides team site sidebar

---

#### Hide Page Title
Hides theme’s page title.

---

#### Hide Footer

Allows you to hide the footer, increasing the available content area. 

---

#### Hide Breadcrumb

Hide Breadcrumb from the SharePoint site.

---

#### Headings

Apply theme heading typefaces to SPFx content and web parts. 