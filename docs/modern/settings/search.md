Settings affecting search

<img src="../../images/bindtuning-settings-search.png" alt="BindTuning Settings Search"	title="BindTuning Settings Search" width="300" />

---

#### Show search results on

- <b>Search page</b>;
- <b>Popup</b>;

---

#### Layout

- <b>Full</b>;
- <b>Compact</b>;

---

#### Placeholder text

Customize the text of the search placeholder

---

#### Maximum search result items
Set maximum number of search result items to be shown inside the search popup