#### Update notifications
- Click the bell icon to get redirected to your theme's my account section where you can update your theme. If there is no update, the bell icon will not appear on the panel.

---

#### Relevant information
- User profile opens menu with links to documentation, change log, license and the My Account section of the BindTuning website.

---

#### Dark Mode

<img src="../../images/settings-darkmode.png" alt="BindTuning Settings Dark Mode"	title="BindTuning Settings Dark Mode" width="300" />

- Click the moon icon to toggle dark mode. This option is stored as a setting so, after you change to the mode you want, click "Apply".
