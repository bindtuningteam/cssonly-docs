<a name="#setupthepreinstallationenvironment"></a>
### 1# Set up the pre-installation environment
To make sure everything goes as smooth as possible there are a few requirements you need to go through before starting the installation process.

1. Open your SharePoint Site;
2. Under **Site Actions**, click on **Manage Site Features**; 
3. Deactivate **Minimal Download Strategy**.


<a name="installthetheme"></a>
### 2# Install the theme 

1. Open your SharePoint Site;
2. Open the **Settings** menu and click on **Site Settings**;

	![sps_installation_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/sps_installation_1.png) 
3. Under Web Designer Galleries, click on **Solutions**;

	<p class="alert-success">If you're not working on your root site, this option will not appear.</p>
	
	![sps_installation_2](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/sps_installation_2.png) 

4. Click on **Upload Solution**;
5. Upload **yourthemename.SPCSS.wsp file**; 
6. Click **OK**; 
7. Click on **Activate** to apply the theme.

<p class="alert-success">After that final step, the theme will be applied to all your sites.</p>