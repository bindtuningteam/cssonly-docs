<a name="resetthethememasterpages"></a>
### 1# Reset the theme Master pages 
Right, lets get busy! Step 1 of uninstalling any BindTuning theme for Sanbdox solution is to reset the theme master pages and switch to one of SharePoint's default master pages (seattle or oslo) before uninstalling the theme. This way your site will keep on displaying your content and you wont get any sticky error.

1. **On your root site**, **open the *Settings* menu** and **click on *Site Settings***;

2. Under *Look and Feel*, **click on *Master page***; 


	**Note:** If you haven't activated SharePoint Publishing Features the Master page option will not appear on the list. We explain how you can activate SharePoint Publishing Features [here](#Change the master page.md).


3. With the option "Specify a master page to be used..." selected, **pick one of SharePoint's default masters (seattle or oslo)** for both the *Site Master Page* and the *System Master Page*; 

4. Also **check the option "Reset all subsites..."** for both the *Site Master Page* and the *System Master Page* in case any of your subsites is using the theme master pages; 

5. **Click *OK.***

Done! Lets move on to deactivating and deleting your theme.

---

<a name="deactivateanddeletethetheme"></a>
### 2# Deactivate and delete the theme on SharePoint

1. On your root site **open the *Settings* menu** and **click on *Site Settings***;

2. Under *Web Design Galleries*, **click on *Solutions***;

3. **Select *yourthemename.SPCSS.wsp* file** and **click on *Deactivate***;

4. **Select *yourthemename.SPCSS.wsp* file again** and **click on *Delete***.

---

<a name="finishingoff"></a>
### 3# Finishing off
Only a few more final steps to completly remove and uninstall your theme. 

1. **Open your site with SharePoint designer** and **open *All Files***;
2. Open _catalogs and **open *masterpage***;
3. **Delete the folder *yourthemename***
4. **Open *All Files*** and **open *Style Library***.
5. **Delete the folder *yourthemename***. 

**Success**: You've now successfully removed and uninstalled your theme.