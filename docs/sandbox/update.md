Before upgrading your theme's version, you will need to request the new version at BindTuning.com and follow the steps on the <a href="../update/" target="_blank">next link</a>.

<p class="alert alert-warning">ALWAYS make a backup of all of your theme assets, including CSS files, master pages, page layouts,... Upgrading the theme will remove all custom changes you have applied previously.</p> 

### Uninstall the theme

To upgrade your theme version, you need to first completely remove the theme from your website. 

You can find the instructions for uninstalling a theme 
<a href="../uninstall/" target="_blank">here</a>.

---

### Install the theme again

After completely removing the theme you can move on to installing the new version.

You can find the instructions for installing a theme <a href="../installation/" target="_blank">here</a>.

Version upgraded! ✅